//
//  CountriesCell.h
//  CountriesTV
//
//  Created by Ronald Campanari on 3/25/15.
//  Copyright (c) 2015 My Project. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountriesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelNameOfCountry;

@end
