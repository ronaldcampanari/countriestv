//
//  CountriesStore.h
//  CountriesTV
//
//  Created by Ronald Campanari on 3/26/15.
//  Copyright (c) 2015 My Project. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountriesStore : NSObject

+ (id)sharedInstance;

- (int)count;
- (NSDictionary *)country;

@property NSArray *countriesArray;
@property NSArray *continentArray;
@property NSArray *populationArray;
@property NSArray *pibArray;
@property NSArray *imagesArray;


@end
