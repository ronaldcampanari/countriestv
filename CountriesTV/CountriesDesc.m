//
//  CountriesDesc.m
//  CountriesTV
//
//  Created by Ronald Campanari on 3/25/15.
//  Copyright (c) 2015 My Project. All rights reserved.
//

#import "CountriesDesc.h"

@implementation CountriesDesc

+ (id)CountryWithName:(NSString *)newName {
    
    return [[CountriesDesc alloc] initWithName:newName];
}

- (id)initWithName:(NSString *)newName  {
    
    self = [super init];
    if (self) {
        _nome = newName;
    }
    return self;
}

@end
