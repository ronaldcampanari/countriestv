//
//  CountriesDesc.h
//  CountriesTV
//
//  Created by Ronald Campanari on 3/25/15.
//  Copyright (c) 2015 My Project. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountriesDesc : NSObject

@property NSString *nome;
@property NSString *continente;
@property NSString *img;
@property float population;
@property float pib;

- (id)initWithName:(NSString *)newName;
+ (id)CountryWithName:(NSString *)newName;

@end
