//
//  CountriesStore.m
//  CountriesTV
//
//  Created by Ronald Campanari on 3/26/15.
//  Copyright (c) 2015 My Project. All rights reserved.
//

#import "CountriesStore.h"
#import "ReceiveCountries.h"
#import "CountriesDesc.h"

@implementation CountriesStore {
    NSMutableDictionary *_country;
    
}

+ (id)sharedInstance {
    
    static CountriesStore *_sharedInstace = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstace = [[CountriesStore alloc] init];
    });
    
    return _sharedInstace;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _country = [[NSMutableDictionary alloc] init];
        [self populaDados];
    }
    return self;
}

- (int)count {
    return (int)_country.count;
}

- (NSDictionary *)country {
    return _country;
}

- (void)populaDados{
    
    NSError *error;
    NSBundle *bundle = [NSBundle mainBundle];
    
    NSArray *files = @[@"images",@"countries", @"continent", @"population", @"pib"];
    
    for(int i=0; i<files.count; i++){
        
        NSString *path = [bundle pathForResource:[files objectAtIndex:i] ofType:@"txt"];
        
        NSString *Desc = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        
        if (i==0)
            _imagesArray = [Desc componentsSeparatedByString:@";"];
        if (i==1)
            _countriesArray = [Desc componentsSeparatedByString:@";"];
        if(i==2)
            _continentArray = [Desc componentsSeparatedByString:@";"];
        if(i==3)
            _populationArray = [Desc componentsSeparatedByString:@";"];
        if(i==4)
            _pibArray = [Desc componentsSeparatedByString:@";"];
    }
    
   
    
    for (int i = 0; i < _imagesArray.count; i++) {
        
        
        CountriesDesc *coun = [[CountriesDesc alloc]init];
        
        coun.nome = [_countriesArray objectAtIndex:i];
        coun.continente = [_continentArray objectAtIndex:i];
        coun.population = [[_populationArray objectAtIndex:i]floatValue];
        coun.pib = [[_pibArray objectAtIndex:i]floatValue];
        coun.img = [_imagesArray objectAtIndex:i];
        
        NSString *countryDictionaryKey = [NSString stringWithFormat:@"%c", [coun.nome characterAtIndex:0]];
        
        NSMutableArray *countryDictionaryArray = [NSMutableArray arrayWithArray:[_country objectForKey:countryDictionaryKey]];
        [countryDictionaryArray addObject:coun];
        [_country setObject:[NSArray arrayWithArray:countryDictionaryArray] forKey:countryDictionaryKey];


        
        
    }
}



@end
