//
//  ReceiveCountries.m
//  CountriesTV
//
//  Created by Ronald Campanari on 3/25/15.
//  Copyright (c) 2015 My Project. All rights reserved.
//

#import "ReceiveCountries.h"
#import "CountriesDesc.h"
#import "CountriesStore.h"

@interface ReceiveCountries (){
    
    BOOL info;
}



@end

@implementation ReceiveCountries

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _labelName.textColor = [UIColor whiteColor];
    _labelContinent.textColor = [UIColor whiteColor];
    _labelPopulation.textColor = [UIColor whiteColor];
    _labelPib.textColor = [UIColor whiteColor];
    
    info = NO;
    
   _imageBackground.image = [self blur:_imageBackground.image];
    _countryValue = nil;
}
- (IBAction)actionList:(id)sender {
    
    info = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(info){
        _labelName.hidden = NO;
        _labelContinent.hidden = NO;
        _labelPopulation.hidden = NO;
        _labelPib.hidden = NO;
        _imageFlag.hidden = NO;
        
    if (_countryValue != nil) {
        _labelName.text = _countryValue.nome;
        _labelContinent.text = [NSString stringWithFormat:@"Continente: %@", _countryValue.continente];;
        _labelPopulation.text  = [NSString stringWithFormat:@"População: %1f", _countryValue.population];
        _labelPib.text  = [NSString stringWithFormat:@"Pib: %f", _countryValue.pib];
        _imageFlag.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", _countryValue.img]];
        }
    }
}

- (UIImage*) blur:(UIImage*)theImage
{
    // ***********If you need re-orienting (e.g. trying to blur a photo taken from the device camera front facing camera in portrait mode)
    // theImage = [self reOrientIfNeeded:theImage];
    
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:theImage.CGImage];
    
    // setting up Gaussian Blur (we could use one of many filters offered by Core Image)
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:10.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage];//create a UIImage for this function to "return" so that ARC can manage the memory of the blur... ARC can't manage CGImageRefs so we need to release it before this function "returns" and ends.
    CGImageRelease(cgImage);//release CGImageRef because ARC doesn't manage this on its own.
    
    return returnImage;
    
    // *************** if you need scaling
    // return [[self class] scaleIfNeeded:cgImage];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
