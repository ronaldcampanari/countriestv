//
//  CountriesTableView.m
//  CountriesTV
//
//  Created by Ronald Campanari on 3/25/15.
//  Copyright (c) 2015 My Project. All rights reserved.
//

#import "CountriesTableView.h"
#import "CountriesDesc.h"
#import "CountriesCell.h"
#import "ReceiveCountries.h"
#import "CountriesStore.h"

@interface CountriesTableView (){
    
    NSDictionary *DB;
    NSArray *DBArray;
    
    
}

    @property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end

@implementation CountriesTableView{
    
    NSString *searchTextInitialLetter;
    NSArray *searchResults;
    
    BOOL searching;

}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    DB = [[CountriesStore sharedInstance] country];
    DBArray = [[DB allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    searching = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    if (searching)
        return 1;
    else
        return [DBArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
 
    return [DBArray objectAtIndex:section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (searching)
        return nil;
    else
        return DBArray;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (searching)
        return [searchResults count];
    else {
        NSString *secTitle = [DBArray objectAtIndex:section];
        NSArray *secCountry = [DB objectForKey:secTitle];
        return [secCountry count];
}
}

- (IBAction)buttonBack:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CountriesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"huehue" forIndexPath:indexPath];
    
    if (searching) {
        
        CountriesDesc *country = [searchResults objectAtIndex:[indexPath row]];
        
        cell.labelNameOfCountry.text = country.nome;
        
    } else {
        // Get the information for the section and the cell.
        NSString *secTitle = [DBArray objectAtIndex:indexPath.section];
        NSArray *secCountry = [DB objectForKey:secTitle];
        CountriesDesc *country = [secCountry objectAtIndex:[indexPath row]];
    
        cell.labelNameOfCountry.text = country.nome;
        
    }
    
    // Return the cell.
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ReceiveCountries *vc = (ReceiveCountries *)[self presentingViewController];
      if(searching){
        vc.countryValue = [searchResults objectAtIndex:indexPath.row];
    }else{
        NSString *secTitle = [DBArray objectAtIndex:indexPath.section];
        NSArray *secCountry = [DB objectForKey:secTitle];
         vc.countryValue = [secCountry objectAtIndex:[indexPath row]];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (BOOL)filterContentForSearchText:(NSString*)searchText
{
    // Verify if searchText is nil or empty.
    if (searchText == nil || [searchText isEqualToString:@""])
        return NO;
    
    // This method will help me with the search.
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"nome contains[cd] %@", searchText];
    
    // Get the key.
    searchTextInitialLetter = [NSString stringWithFormat:@"%c", [searchText characterAtIndex:0]];
    
    // Get the array related to the key.
    NSArray *elementsToBeFiltered = [DB objectForKey:searchTextInitialLetter];
    
    // Verify if section exist.
    if ([elementsToBeFiltered count] > 0) {
        // Apply the search predicate and return YES for success.
        searchResults = [elementsToBeFiltered filteredArrayUsingPredicate:resultPredicate];
        
        // Set initial letter to nil to disable index if nothing has found.
        if ([searchResults count] == 0)
            searchTextInitialLetter = nil;
        
        return YES;
    } else
        return NO;
    
}

//- (IBAction)dismissKeyboard:(id)sender {
//    // Close the keyboard.
//    [_searchBar resignFirstResponder];
//}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // Close keyboard.
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    // Close keyboard.
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // Show cancel button.
    [searchBar setShowsCancelButton:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    // Hidde cancel button.
    [searchBar setShowsCancelButton:NO];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // Get search texts.
    if ([self filterContentForSearchText:[searchBar text]])
        searching = YES;
    else
        searching = NO;
    
    [[self tableView] reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




@end
