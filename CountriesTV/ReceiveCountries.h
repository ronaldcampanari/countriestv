//
//  ReceiveCountries.h
//  CountriesTV
//
//  Created by Ronald Campanari on 3/25/15.
//  Copyright (c) 2015 My Project. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CountriesDesc;

@interface ReceiveCountries : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imageFlag;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelContinent;
@property (weak, nonatomic) IBOutlet UILabel *labelPopulation;
@property (weak, nonatomic) IBOutlet UILabel *labelPib;

@property CountriesDesc *countryValue;

@end
